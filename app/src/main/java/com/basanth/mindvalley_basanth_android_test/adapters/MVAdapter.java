package com.basanth.mindvalley_basanth_android_test.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.basanth.mindvalley_basanth_android_test.cache.MVCache;
import com.basanth.mindvalley_basanth_android_test.R;
import com.basanth.mindvalley_basanth_android_test.models.User;
import com.basanth.mindvalley_basanth_android_test.models.UserProfile;

import java.util.List;

/**
 * Created by Basanth on 10/8/2016.
 */

public class MVAdapter extends RecyclerView.Adapter<MVAdapter.MovieViewHolder> {

    private List<User> userList;
    private int rowLayout;


    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        LinearLayout moviesLayout;
        TextView userName;
        TextView name;
        ImageView profilePic;

        public MovieViewHolder(View v) {
            super(v);
            moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
            userName = (TextView) v.findViewById(R.id.tv_userName);
            name = (TextView) v.findViewById(R.id.tv_name);
            profilePic = (ImageView) v.findViewById(R.id.iv_profileImage);
        }
    }

    public MVAdapter(List<User> userList, int rowLayout) {
        this.userList = userList;
        this.rowLayout = rowLayout;
    }

    @Override
    public MVAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MovieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {

        if (userList.get(position).getUser()!=null)
        {
            UserProfile userProfile = userList.get(position).getUser();
            if (userProfile.getProfileImage().getMedium()!=null)
                MVCache.getInstance().setImageFromURL(  userProfile.getProfileImage().getMedium(),holder.profilePic);
            if (userProfile.getUsername()!=null)
                holder.userName.setText(userProfile.getUsername());
            if (userProfile.getName()!=null)
                holder.name.setText(userProfile.getName());
        }

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}