package com.basanth.mindvalley_basanth_android_test.services;

import com.basanth.mindvalley_basanth_android_test.models.User;
import com.basanth.mindvalley_basanth_android_test.models.UserProfile;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by Basanth on 10/9/2016.
 */

public interface ApiInterface {
    @GET("wgkJgazE")
    Call<List<User>> getUserProfiles();
}