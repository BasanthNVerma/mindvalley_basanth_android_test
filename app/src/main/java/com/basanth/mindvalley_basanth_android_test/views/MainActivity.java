package com.basanth.mindvalley_basanth_android_test.views;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.basanth.mindvalley_basanth_android_test.cache.MVCache;
import com.basanth.mindvalley_basanth_android_test.adapters.MVAdapter;
import com.basanth.mindvalley_basanth_android_test.R;
import com.basanth.mindvalley_basanth_android_test.models.User;
import com.basanth.mindvalley_basanth_android_test.services.ApiClient;
import com.basanth.mindvalley_basanth_android_test.services.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "MainActivity";

    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Recycler view to demonstrate image cache
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Simple Swipe to refresh integration
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        //Fab to demonstrate Generic File Download and caching
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MVCache.getInstance().getFileFromURL("http://pastebin.com/raw/wgkJgazE")==null)
                    Snackbar.make(view, "Downloading the File since it's not found in Cache.", Snackbar.LENGTH_LONG).show();
                else
                    Snackbar.make(view, "File already Cached, not downloading.", Snackbar.LENGTH_LONG).show();
            }
        });


        //Service to fetch data from the Pastebin test link
        apiService  = ApiClient.getClient().create(ApiInterface.class);
        fetchAndPopulateData();
    }

    void refreshItems() {
        fetchAndPopulateData();
        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    void fetchAndPopulateData()
    {
        Call<List<User>> call = apiService.getUserProfiles();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                mRecyclerView.setAdapter(new MVAdapter(response.body(), R.layout.list_item));
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });

    }
}
